<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TncPromoHasTag extends Model
{
    protected $table = "tnc_promo_hastag";

    // protected $primaryKey = "promo_hastag_building_id";

    protected $fillable = [];

    protected $hidden = [
        'promo_hastag_create_date',
        'promo_hastag_create_by',
        'promo_hastag_update_date',
        'promo_hastag_update_by',
        'promo_hastag_del_status'
    ];

    public function roomType()
    {
        return $this->belongsTo(RoomType::class, 'tncpromo_roomtype_id', 'roomtype_id');
    }
}
