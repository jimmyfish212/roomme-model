<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';

    protected $primaryKey = 'rooms_id';

    public function roomType()
    {
        return $this->belongsTo(RoomType::class);
    }
}
