<?php

namespace App\Models;

use App\Models\Building;
use Illuminate\Database\Eloquent\Model;

class TenantStatus extends Model
{
    protected $primaryKey = 'tenant_stat_id';

    protected $table = 'tenant_status';

    public function booking()
    {
        return $this->hasMany(Booking::class, 'tenant_stat_id', 'book_tenant_status');
    }
}
