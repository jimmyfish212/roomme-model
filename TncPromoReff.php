<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TncPromoReff extends Model
{
    protected $table = "tnc_promo_reff";

    protected $primaryKey = "tncpromoreff_id";

    protected $hidden = [
        'tncpromoreff_del_status',
        'tncpromoreff_update_date',
        'tncpromoreff_update_by',
        'tncpromoreff_create_date',
        'tncpromoreff_create_by',
        'tncpromo_roomtype_id'
    ];

    protected $fillable = [];

    public function roomType()
    {
        return $this->belongsTo(RoomType::class, 'tncpromo_roomtype_id', 'roomtype_id');
    }
}
