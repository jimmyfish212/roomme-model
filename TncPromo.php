<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TncPromo extends Model
{
    protected $table = "tnc_promo";

    protected $primaryKey = "tncpromo_id";

    protected $fillable = [];

    protected $hidden = [
        'tncpromo_create_by',
        'tncpromo_create_date',
        'tncpromo_update_by',
        'tncpromo_update_date',
        'tncpromo_del_status',
    ];

    public function roomType()
    {
        return $this->belongsTo(RoomType::class, 'tncpromo_roomtype_id', 'roomtype_id');
    }

    public function building()
    {
        return $this->belongsTo(Building::class, '');
    }
}
