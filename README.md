Modular Model
-

This is repository where model were stored, please don't deploy it outside the project folder!   

### How to deploy ?

#### Cloning method   
1. `cd app; rm User.php`   
2. `git clone {repository-url} Models`   
3. `cd ../..`   
4. `composer dump-autoload`   

#### Init method
1. `cd app; rm User.php`   
2. `mkdir Models; cd Models`   
3. `git init`   
4. `git remote add origin {repository-url}`   
5. `git pull origin master`   
6. `cd ../..`   
7. `composer dump-autoload`
