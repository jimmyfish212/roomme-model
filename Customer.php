<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customer extends Model implements JWTSubject
{
    protected $table = "customer";
    protected $primaryKey = 'cust_id';
    const CREATED_AT = 'cust_create_date';
    const UPDATED_AT = 'cust_update_date';

    protected $fillable = [
        'cust_hexid',
        'cust_name',
        'cust_gender',
        'cust_image',
        'cust_photo_idcard',
        'cust_nik',
        'cust_address',
        'cust_dob',
        'cust_place_of_birth',
        'cust_phone',
        'cust_email',
        'cust_password',
        'cust_hashkey',
        'cust_login_multidevice',
        'cust_login_hashkey',
        'cust_login_hashpass',
        'cust_facebook_id',
        'cust_google_id',
        'cust_facebook_url',
        'cust_google_url',
        'cust_device',
        'cust_member_type_id',
        'cust_id_refferal',
        'cust_is_active',
        'cust_create_by',
        'cust_create_date',
        'cust_update_by',
        'cust_update_date',
        'cust_del_status'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'cust_id', 'book_cust_id');
    }
}
