<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Promo extends Model
{
    protected $table = "promo";
    protected $primaryKey = 'promo_id';
    const CREATED_AT = 'promo_create_date';
    const UPDATED_AT = 'promo_update_date';
}
