<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Facilities extends Model
{
    protected $primaryKey = "facilities_id";

    protected $table = 'facilities';

    protected $hidden = [
        'facilities_id',
        'facilities_ref_id',
        'facilities_price',
        'facilities_create_date',
        'facilities_create_by',
        'facilities_update_by',
        'facilities_update_date',
        'facilities_del_status',
        'facilities_desc'
    ];

    public function buildings()
    {
        return $this->belongsTo(Building::class, 'facilities_ref_id', 'build_id');
    }
}
