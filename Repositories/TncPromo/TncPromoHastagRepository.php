<?php


namespace App\Models\Repositories\TncPromo;


use App\Models\TncPromoHasTag;

class TncPromoHastagRepository implements TncPromoHastagRepositoryInterface
{
    private $model;

    public function __construct(TncPromoHasTag $model)
    {
        $this->model = $model;
    }

    public function getActiveLongstayByBuildId(int $buildId)
    {
        return $this->model
            ->where('promo_hastag_del_status', '0')
            ->where('promo_hastag_building_id', $buildId);
    }
}
