<?php


namespace App\Models\Repositories\TncPromo;


use App\Models\RoomType;

interface TncPromoRepositoryInterface
{
    public function getActiveLongstayByRoomType($roomTypeId);

    public function getActiveLongstayByBuildId($buildId);

    public function getActivePeriodicByRoomTypeId($roomTypeId);

    public function getActivePeriodicByRoomTypeObject(RoomType $roomType);
}
