<?php


namespace App\Models\Repositories\TncPromo;


interface TncPromoHastagRepositoryInterface
{
    public function getActiveLongstayByBuildId(int $buildId);
}
