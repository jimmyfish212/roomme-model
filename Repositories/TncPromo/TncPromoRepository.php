<?php


namespace App\Models\Repositories\TncPromo;


use App\Models\RoomType;
use App\Models\TncPromo;

class TncPromoRepository implements TncPromoRepositoryInterface
{
    private $model;

    public function __construct(TncPromo $model)
    {
        $this->model = $model;
    }

    public function getActiveLongstayByRoomType($roomTypeId)
    {
        return $this->model->where('tncpromo_flag', 0)
            ->where('tncpromo_del_status', '0')
            ->where('tncpromo_roomtype_id', $roomTypeId);
    }

    public function getActiveLongstayByBuildId($buildId)
    {
        return $this->model->where('tncpromo_flag', 0)
            ->where('tncpromo_del_status', '0')
            ->where('tncpromo_building_id', $buildId);
    }

    public function getActivePeriodicByRoomTypeId($roomTypeId)
    {
        return $this->model->where('tncpromo_flag', 1)
            ->where('tncpromo_del_status', '0')
            ->where('tncpromo_roomtype_id', $roomTypeId);
    }

    public function getActivePeriodicByRoomTypeObject(RoomType $roomType)
    {
        return $roomType->tncPromo()
            ->where('tncpromo_flag', 1)
            ->where('tncpromo_del_status', '0');
    }
}
