<?php


namespace App\Models\Repositories\Room;


use App\Models\Repositories\Booking\BookingRepositoryInterface;
use App\Models\Room;
use App\Models\RoomType;

class RoomRepository implements RoomRepositoryInterface
{
    private $model;
    private $bookingRepository;

    public function __construct
    (
        Room $model,
        BookingRepositoryInterface $bookingRepository
    )
    {
        $this->model = $model;
        $this->bookingRepository = $bookingRepository;
    }

    public function getRoomAvailable(RoomType $roomType, $checkIn, $checkOut)
    {
        $bookingList = $this->bookingRepository->getBookingByCheckInAndOutWithActiveStatus($checkIn, $checkOut)
            ->pluck('book_room_id');

        return $this->model
                ->where('rooms_type_id', $roomType->roomtype_id)
                ->where('rooms_status_id', 1)
                ->whereNotIn('rooms_id', $bookingList);
    }

    public function getRoomAvailabilityCount(RoomType $roomType, $checkIn, $checkOut)
    {
        return $this->getRoomAvailable($roomType, $checkIn, $checkOut)->count();
    }

    public function getOneRoomAvailable(RoomType $roomType, $checkIn, $checkOut)
    {
        return $this->getRoomAvailable($roomType, $checkIn, $checkOut)->first();
    }
}
