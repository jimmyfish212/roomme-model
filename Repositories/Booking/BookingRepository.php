<?php


namespace App\Models\Repositories\Booking;


use App\Models\Booking;

class BookingRepository implements BookingRepositoryInterface
{
    private $model;

    public function __construct(Booking $model)
    {
        $this->model = $model;
    }

    public function getBookingByCheckInAndOutWithActiveStatus($checkIn, $checkOut)
    {
        return $this->model->whereBetween('book_check_in', [
            $checkIn, $checkOut
        ])->whereIn('book_tenant_status', [1,2,3,7])->get();
    }

    public function getLastBookingId()
    {
        return $this->model->max('book_id');
    }

    public function updateCancelUnpaidBookDeposit($custId)
    {
        $this->model
            ->where('book_cust_id', $custId)
            ->where('book_tenant_status', 1)
            ->where('book_payment_status_id', 1)
            ->update([
                'book_tenant_status' => 8, 
                'book_payment_status_id' => 5,
                'book_update_by' => 'WEBSITE-BOOKING'
            ]);

        return $this->model;
    }

    public function storeBooking(array $params)
    {
        $this->model->insert($params);

        $this->model = $this->model->where('book_no', $params['book_no'])->first();
        return $this->model;
    }

    public function getAllBooking($custId)
    {
        return $this->model
            ->where('book_cust_id', $custId)
            ->get();
    }

    public function getBookingById($bookingId)
    {
        return $this->model
            ->where('book_id', $bookingId)
            ->first();
    }

    public function getBookingByCode($bookingCode)
    {
        return $this->model
            ->where('book_no', $bookingCode)
            ->first();
    }

    public function updateVANumber($vaNumber)
    {
        $this->model
            ->where('book_va_no', $vaNumber)
            ->update([
                'book_va_no' => 'EXPIRED',
                'book_update_by' => 'PAYMENT-VALIDATION'
            ]);

        return $this->model;
    }

    public function updateBookingPaymentStatus(array $params, $bookingId)
    {
        $this->model
            ->where('book_id', $bookingId)
            ->update($params);

        $this->model = $this->model->where('book_id', $bookingId)->first();

        return $this->model;
    }
}
