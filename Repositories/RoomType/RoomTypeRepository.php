<?php


namespace App\Models\Repositories\RoomType;


use App\Models\Repositories\Booking\BookingRepositoryInterface;
use App\Models\RoomType;

class RoomTypeRepository implements RoomTypeRepositoryInterface
{
    private $model;
    private $bookingRepository;

    public function __construct(
        RoomType $roomType,
        BookingRepositoryInterface $bookingRepository
    )
    {
        $this->model = $roomType;
        $this->bookingRepository = $bookingRepository;
    }

    public function getRoomTypeById($id)
    {
        $this->model = $this->model
            ->where('roomtype_id', $id);

        return $this->model->first();
    }
}
