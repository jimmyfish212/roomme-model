<?php


namespace App\Models\Repositories\Promo;

use App\Models\Promo;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PromoRepository implements PromoRepositoryInterface
{
    private $model;

    public function __construct(Promo $model)
    {
        $this->model = $model;
    }

    public function getActiveDataByAffiliate($affiliate)
    {
        return $this->model->where('promo_validation', $affiliate)
                            ->whereDate('promo_start_date', '<=', Carbon::now())
                            ->whereDate('promo_end_date', '>=', Carbon::now())
                            ->where('promo_del_status','0')
                            ->first();
    }
}
