<?php


namespace App\Models\Repositories\Register;

use Illuminate\Support\Collection;

interface RegisterRepositoryInterface
{
    public function getDataByEmail($email);

    public function getDataByEmailOrPhone($email,$phone);

    public function getDataByToken($token);

    public function storeData(array $data);

    public function deleteData($id);

    public function softDeleteData($id);
}
